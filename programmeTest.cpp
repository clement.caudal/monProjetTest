#include <stdio.h>
#include <iostream>
using namespace std;

//---------------------------------------
// CAUDAL Clément & DEVELOPPER Donovan Laget  programmeTest.cpp version 1.1
//---------------------------------------

int main()
{
	//Fichier princiapl de test sur Git avec l'utilisation de lignes de commande

	// On souhaite effectuer un calcul et afficher le résultat

	int variableA, variableB, resultat;

	cout<<"Variable A : "<<endl;
	cin>>variableA;

	cout<<"Variable B : "<<endl;
	cin>>variableB;

	// Calcul de l'addition des variables A et B
	resultat = variableA + variableB;

	// Affichage du résultat
	cout<<"Le resultat de l'addition de "<<variableA<<" et "<<variableB<<" est : "<<resultat;

	// Ajout d'un commentaire version 1.1
}

